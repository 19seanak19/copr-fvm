# copr-fvm

This repo contains what's required for a fvm (Flutter Version Manager) rpm, hosted on Copr. 

This is an unofficial packaging, so please do not bother the fvm developers with any issues from using this package.

## Installing

```
dnf copr enable 19seanak19/fvm
dnf install fvm
```

## Uninstalling

```
dnf uninstall fvm
dnf copr disable 19seanak19/fvm
```

## Updating the package

This is more here to help me update, but this is how to update to a new fvm version.

1. Update the "Version" field in `fvm.spec`
2. Update the "FVm_VERSION" variable in `.copr/Makefile`