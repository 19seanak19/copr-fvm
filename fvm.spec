Name:           fvm
Version:        2.4.1
Release:        1%{?dist}
Summary:        Flutter Version Manager

License:        MIT
URL:            https://fvm.app/

Source0:        https://github.com/fluttertools/fvm/archive/%{version}.tar.gz
%global         SHA256SUM0 578ddc4cc63656938afbfa971e08325d22af1ce4487cb264753dd829e419646b

ExcludeArch:    s390x ppc64

BuildRequires:  dart

%ifarch %{ix86} %{arm}
Requires:       dart
%endif

%description
The Flutter Version Manager for installing and managing multiple
versions of the Flutter SDK. This is an unofficial package not
not affiliated with the tool's developers.

%global debug_package %{nil}
%define _build_id_links none
%ifarch x86_64 aarch64
%define __os_install_post %{nil}
%endif

%prep
echo "%SHA256SUM0  %SOURCE0" | sha256sum -c -
%autosetup

%build
echo 'Building for %{_arch}'
dart pub get
%ifarch x86_64 aarch64
dart compile exe -Dversion=%{version} -o fvm bin/main.dart
%endif
%ifarch %{ix86} %{arm}
dart compile jit-snapshot -Dversion=%{version} -o fvm.dart.app.snapshot bin/main.dart
echo -e '#!/bin/sh\nexec dart %{_libdir}/fvm.dart.app.snapshot' >> fvm
chmod +x fvm
%endif

%install
mkdir -p %{buildroot}%{_bindir}
mv fvm %{buildroot}%{_bindir}/fvm
%ifarch %{ix86} %{arm}
mkdir -p %{buildroot}%{_libdir}
mv fvm.dart.app.snapshot %{buildroot}%{_libdir}/fvm.dart.app.snapshot
%endif

%check
%ifarch x86_64 aarch64
%{buildroot}%{_bindir}/fvm
%endif
%ifarch %{ix86} %{arm}
dart %{buildroot}%{_libdir}/fvm.dart.app.snapshot
%endif

%files
%{_bindir}/fvm
%ifarch %{ix86} %{arm}
%{_libdir}/fvm.dart.app.snapshot
%endif
%license LICENSE

%changelog
* Sun Nov 20 2022 Sean Kimball - 2.4.1-1
- Initial package creation